# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "b" {
  bucket = var.s3_user_bucket_name
  acl    = "private"
  force_destroy = true

  tags = {
    Name        = var.s3_user_bucket_name
    Environment = "Dev"
  }
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "job_offers" {
  bucket = var.s3_user_bucket_name
  key = "job_offers/raw/"
  source = "/dev/null"

}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.b.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_s3_bucket]
}